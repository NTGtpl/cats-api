import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Арвлпрктсратмоемраова', description: '', gender: 'male' }];
let catId: number;
const description = "Добавлено описание";
const HttpClient = Client.getInstance();

describe('API core save description', () => {
  beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else {
        throw new Error('Не получилось получить id тестового котика!');
      }
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  it('Добавить описание котику', async () => {
    const response = await HttpClient.post(`core/cats/save-description`, {
      responseType: 'json',
      json: {
        catId: catId,
        catDescription: description
      },
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toEqual({
      id: catId,
      name: cats[0].name,
      description: description,
      gender: cats[0].gender,
      tags: null,
      likes: 0,
      dislikes: 0,
    });
  });
});