import Client from '../../dev/http-client';
import { Cat, CatMinInfo, CatsList} from '../../dev/types';

const HttpClient = Client.getInstance();

describe('API core all by letter', () => {
    it('Метод получения сгруппированного списка котов', async () => {
        const response = await HttpClient.get('core/cats/allByLetter', {
            responseType: 'json',
        });
        expect(response.statusCode).toEqual(200);

        expect(response.body).toEqual({
            groups: expect.arrayContaining([
                expect.objectContaining({
                    title: expect.any(String),
                    cats: expect.arrayContaining([
                        expect.objectContaining({
                            id: expect.any(Number),
                            name: expect.any(String),
                            description: expect.any(String),
                            gender: expect.any(String),
                            likes: expect.any(Number),
                            dislikes: expect.any(Number),
                            count_by_letter: expect.any(String)
                        })
                    ]),
                    count_in_group: expect.any(Number),
                    count_by_letter: expect.any(Number),
                }),
            ]),
            count_output: expect.any(Number),
            count_all: expect.any(Number),
        });
    });
});
